Author
===========
 + Tom Rothwell
Original concept @ http://drupal.org/node/117140#comment-4309566

Description
===========
Adds the ability to navigate through nodes using a previous next style per content type.

Installing
============
1) Copy 'domain_previous_next' module to sites/all/modules/
2) Configure permissions, set 'access previous/next' to roles that should have access
3) Select the content types to display on under '/admin/settings/domain-previous-next/'
4) Set where the block should sit 'Domain Previous/Next Links'

Requirements
============
 + Domain Access - This is only required to use the per domain functionality.
 
Theming
============
A tpl file is provided for overriding the default output, look inside the file to
find the variables available for use.

Future
------------
I'm open for suggestions on enhancements, please feel free to create a ticket.
- Potentially add support for displaying unpublished nodes for admins or specifc roles
- Performance enhancements
- D7 support