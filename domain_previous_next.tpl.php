<?php
/**
 * @file
 * Alter the default display of the output for the previous & next link
 *
 * Available variables:
 * - $previous_link
 * - $next_link
 * - $extra - Contains the full node contents
 * This could be used to output an CCK field.
 */
?>
<div class="previous-next">
	<?php if ($previous_link) : ?>
		<p class="previous">
			<span class="title">
				<?php echo $previous_link['title']; ?>
			</span>
			<?php echo l('Previous Article', 'node/' . $previous_link['id']); ?>
		</p>
	<?php endif; ?>
	
	<?php if ($next_link) : ?>
		<p class="next">
			<span class="title">
				<?php echo $next_link['title']; ?>
			</span>
			<?php echo l('Next Article', 'node/' . $next_link['id']); ?>
		</p>
	<?php endif; ?>
</div>