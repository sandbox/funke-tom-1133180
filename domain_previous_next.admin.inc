<?php
/**
 * @file
 * Drupal Module: Domain Previous/Next - Admin
 * Adds the ability to configure node types per domain
 */

/**
 * Admin form for content type selections per domain
 */
function domain_previous_next_admin_form($form_state, $user_submitted = FALSE) {
  $form = array();
  $current_site_name = variable_get('site_name', 'Drupal');
  
  //check if domain access module exists first
  if (module_exists('domain')) {
    $domains = domain_domains();
  } 
  else {
    //add default domain details
    $domains = array(
      'default' => array(
        'domain_id' => 0, //0 is always used for the default domain
        'sitename' => $current_site_name,
      ),
    );
  }
  
  foreach ($domains as $domain) {
    //dont collapse the current domain
    if ($current_site_name == $domain['sitename']) {
      $collapsed = FALSE;
    }
    else {
      $collapsed = TRUE;
    }
    
    $domain_id = $domain['domain_id'];
    $form['domain_group_' . $domain['sitename']] = array(
      '#type' => 'fieldset',
      '#title' => t($domain['sitename']),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
      '#description' => t("<p>Check the content types that should use the Previous/Next functionality for {$domain['sitename']}.</p>")
    );

   $form['domain_group_' . $domain['sitename']][$domain_id] = array(
      '#type' => 'checkboxes',
      '#title' => t('Node types'),
      '#options' => node_get_types('names'),
      '#default_value' => _domain_previous_next_values($domain_id)
   );
   
  
  }
  
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save Selections'));
  
  return $form;
}

/**
 * Helper - Returns the selected node types
 * @return array()
 */
function _domain_previous_next_values($domain_id) {
  $query = db_query("SELECT node_type FROM {domain_previous_next} WHERE domain_id = '%s'", $domain_id);
  
  while ($result = db_fetch_object($query)) {
    $values[$result->node_type] = $result->node_type;
  }
  
  if ($values) {
    return $values;
  }
  
  return array();
}

/**
 * Form handler
 */
function domain_previous_next_admin() {
  return drupal_get_form('domain_previous_next_admin_form');
}

/**
 * Save the configuration to the database
 */
function domain_previous_next_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $query = db_query('SELECT * FROM {domain_previous_next}');
  
  //build array of results to check against
  while ($result = db_fetch_array($query)) {
    $row[$result['domain_id']] = $result;
  }

  foreach ($values as $domain_id => $selections) {
    if (!is_array($selections)) {
      continue;
    }
    
    foreach ($selections as $key => $value) {
      if ($value) {
        //insert node into db if selected
        db_query("INSERT INTO {domain_previous_next} (domain_id, node_type) VALUES (%d, '%s')", $domain_id, $key);
      } 
      elseif (!$value && isset($row[$domain_id]['node_type'][$key])) {
        //remove node from db if not selected
        db_query("DELETE FROM {domain_previous_next} WHERE domain_id = %d AND node_type = '%s'", $domain_id, $key);
      }
    }
  }
  
  drupal_set_message('Saved selections.');
}